<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A49011">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The address of the Lord Mayor, Aldermen, and Commons of the city of London in Common-Council assembled presented to Her Majesty at Whitehall, August 17. 1693.</title>
    <author>City of London (England). Court of Common Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A49011 of text R40016 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing L2861A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A49011</idno>
    <idno type="STC">Wing L2861A</idno>
    <idno type="STC">ESTC R40016</idno>
    <idno type="EEBO-CITATION">18659749</idno>
    <idno type="OCLC">ocm 18659749</idno>
    <idno type="VID">108109</idno>
    <idno type="PROQUESTGOID">2248552685</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A49011)</note>
    <note>Transcribed from: (Early English Books Online ; image set 108109)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1657:5)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The address of the Lord Mayor, Aldermen, and Commons of the city of London in Common-Council assembled presented to Her Majesty at Whitehall, August 17. 1693.</title>
      <author>City of London (England). Court of Common Council.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by Edward Jones in the Savoy,</publisher>
      <pubPlace>[London] :</pubPlace>
      <date>1693.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Published by authority."</note>
      <note>Reproduction of original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>William -- III, -- King of England, 1650-1702.</term>
     <term>Great Britain -- History -- William and Mary, 1689-1702.</term>
     <term>London (England) -- History -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The address of the Lord Mayor, aldermen, and Commons of the City of London in Common-Council assembled. Presented to Her Majesty at Whitehall, August</ep:title>
    <ep:author>Corporation of London . Court of Common Council</ep:author>
    <ep:publicationYear>1693</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>448</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-12</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-02</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change><date>2008-12</date><label>John Pas</label>
        Sampled and proofread
      </change>
   <change><date>2008-12</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A49011-t">
  <body xml:id="A49011-e0">
   <div type="text" xml:id="A49011-e10">
    <pb facs="tcp:108109:1" rend="simple:additions" xml:id="A49011-001-a"/>
    <head xml:id="A49011-e20">
     <w lemma="the" pos="d" xml:id="A49011-001-a-0010">THE</w>
     <w lemma="address" pos="n1" xml:id="A49011-001-a-0020">ADDRESS</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0030">OF</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-0040">THE</w>
     <hi xml:id="A49011-e30">
      <w lemma="lord" pos="n1" xml:id="A49011-001-a-0050">Lord</w>
      <w lemma="mayor" pos="n1" xml:id="A49011-001-a-0060">Mayor</w>
      <pc xml:id="A49011-001-a-0070">,</pc>
      <w lemma="alderman" pos="n2" reg="aldermans" xml:id="A49011-001-a-0080">Aldermen</w>
     </hi>
     <pc rend="follows-hi" xml:id="A49011-001-a-0090">,</pc>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-0100">and</w>
     <w lemma="commons" pos="n2" rend="hi" xml:id="A49011-001-a-0110">Commons</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0120">of</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-0130">the</w>
     <w lemma="city" pos="n1" xml:id="A49011-001-a-0140">City</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0150">of</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A49011-001-a-0160">London</w>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-0170">in</w>
     <w lemma="common-council" pos="n1" xml:id="A49011-001-a-0180">Common-Council</w>
     <w lemma="assemble" pos="vvn" xml:id="A49011-001-a-0190">Assembled</w>
     <pc unit="sentence" xml:id="A49011-001-a-0200">.</pc>
    </head>
    <opener xml:id="A49011-e60">
     <dateline xml:id="A49011-e70">
      <w lemma="present" pos="vvn" xml:id="A49011-001-a-0210">Presented</w>
      <w lemma="to" pos="acp" xml:id="A49011-001-a-0220">to</w>
      <w lemma="her" pos="po" xml:id="A49011-001-a-0230">Her</w>
      <w lemma="majesty" pos="n1" xml:id="A49011-001-a-0240">Majesty</w>
      <w lemma="at" pos="acp" xml:id="A49011-001-a-0250">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A49011-001-a-0260">Whitehall</w>
      <pc xml:id="A49011-001-a-0270">,</pc>
      <date xml:id="A49011-e90">
       <w lemma="August" pos="nn1" rend="hi" xml:id="A49011-001-a-0280">August</w>
       <w lemma="17." pos="crd" xml:id="A49011-001-a-0290">17.</w>
       <w lemma="1693." pos="crd" xml:id="A49011-001-a-0300">1693.</w>
       <pc unit="sentence" xml:id="A49011-001-a-0310"/>
      </date>
     </dateline>
     <w lemma="publish" pos="j-vn" xml:id="A49011-001-a-0320">Published</w>
     <w lemma="by" pos="acp" xml:id="A49011-001-a-0330">by</w>
     <w lemma="authority" pos="n1" xml:id="A49011-001-a-0340">Authority</w>
     <pc unit="sentence" xml:id="A49011-001-a-0350">.</pc>
     <salute xml:id="A49011-e110">
      <w lemma="to" pos="prt" xml:id="A49011-001-a-0360">To</w>
      <w lemma="the" pos="d" xml:id="A49011-001-a-0370">the</w>
      <w join="right" lemma="queen" pos="n1" xml:id="A49011-001-a-0380">Queen</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A49011-001-a-0381">'s</w>
      <w lemma="most" pos="avs-d" xml:id="A49011-001-a-0390">Most</w>
      <w lemma="excellent" pos="j" xml:id="A49011-001-a-0400">Excellent</w>
      <w lemma="majesty" pos="n1" xml:id="A49011-001-a-0410">Majesty</w>
      <pc xml:id="A49011-001-a-0420">,</pc>
     </salute>
    </opener>
    <p xml:id="A49011-e120">
     <w lemma="we" pos="pns" xml:id="A49011-001-a-0430">WE</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-0440">Your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A49011-001-a-0450">Majesties</w>
     <w lemma="most" pos="avs-d" xml:id="A49011-001-a-0460">most</w>
     <w lemma="dutiful" pos="j" xml:id="A49011-001-a-0470">Dutiful</w>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-0480">and</w>
     <w lemma="loyal" pos="j" xml:id="A49011-001-a-0490">Loyal</w>
     <w lemma="subject" pos="n2" xml:id="A49011-001-a-0500">Subjects</w>
     <pc xml:id="A49011-001-a-0510">,</pc>
     <w lemma="the" pos="d" xml:id="A49011-001-a-0520">the</w>
     <hi xml:id="A49011-e130">
      <w lemma="lord" pos="n1" xml:id="A49011-001-a-0530">Lord</w>
      <w lemma="mayor" pos="n1" xml:id="A49011-001-a-0540">Mayor</w>
      <pc xml:id="A49011-001-a-0550">,</pc>
      <w lemma="alderman" pos="n2" reg="aldermans" xml:id="A49011-001-a-0560">Aldermen</w>
     </hi>
     <pc rend="follows-hi" xml:id="A49011-001-a-0570">,</pc>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-0580">and</w>
     <w lemma="commons" pos="n2" rend="hi" xml:id="A49011-001-a-0590">Commons</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0600">of</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-0610">the</w>
     <w lemma="city" pos="n1" xml:id="A49011-001-a-0620">City</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0630">of</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A49011-001-a-0640">London</w>
     <pc xml:id="A49011-001-a-0650">,</pc>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-0660">in</w>
     <w lemma="common-council" pos="n1" xml:id="A49011-001-a-0670">Common-Council</w>
     <w lemma="assemble" pos="vvn" xml:id="A49011-001-a-0680">Assembled</w>
     <pc xml:id="A49011-001-a-0690">,</pc>
     <w lemma="out" pos="av" xml:id="A49011-001-a-0700">out</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0710">of</w>
     <w lemma="a" pos="d" xml:id="A49011-001-a-0720">a</w>
     <w lemma="deep" pos="j" xml:id="A49011-001-a-0730">deep</w>
     <w lemma="sense" pos="n1" xml:id="A49011-001-a-0740">Sense</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0750">of</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-0760">the</w>
     <w lemma="infinite" pos="j" xml:id="A49011-001-a-0770">Infinite</w>
     <w lemma="goodness" pos="n1" xml:id="A49011-001-a-0780">Goodness</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0790">of</w>
     <w lemma="GOD" pos="nn1" rend="hi" xml:id="A49011-001-a-0800">GOD</w>
     <w lemma="to" pos="acp" xml:id="A49011-001-a-0810">to</w>
     <w lemma="this" pos="d" xml:id="A49011-001-a-0820">this</w>
     <w lemma="nation" pos="n1" xml:id="A49011-001-a-0830">Nation</w>
     <pc xml:id="A49011-001-a-0840">,</pc>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-0850">in</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-0860">the</w>
     <w lemma="signal" pos="n1" xml:id="A49011-001-a-0870">Signal</w>
     <w lemma="deliverance" pos="n1" xml:id="A49011-001-a-0880">Deliverance</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-0890">of</w>
     <w lemma="his" pos="po" xml:id="A49011-001-a-0900">His</w>
     <hi xml:id="A49011-e170">
      <w lemma="sacred" pos="j" xml:id="A49011-001-a-0910">Sacred</w>
      <w lemma="majesty" pos="n1" xml:id="A49011-001-a-0920">Majesty</w>
     </hi>
     <w lemma="from" pos="acp" xml:id="A49011-001-a-0930">from</w>
     <w lemma="those" pos="d" xml:id="A49011-001-a-0940">those</w>
     <w lemma="danger" pos="n2" xml:id="A49011-001-a-0950">Dangers</w>
     <w lemma="to" pos="acp" xml:id="A49011-001-a-0960">to</w>
     <w lemma="which" pos="crq" xml:id="A49011-001-a-0970">which</w>
     <w lemma="he" pos="pns" xml:id="A49011-001-a-0980">he</w>
     <w lemma="have" pos="vvz" xml:id="A49011-001-a-0990">hath</w>
     <w lemma="so" pos="av" xml:id="A49011-001-a-1000">so</w>
     <w lemma="late" pos="av-j" xml:id="A49011-001-a-1010">lately</w>
     <pc xml:id="A49011-001-a-1020">,</pc>
     <w lemma="for" pos="acp" xml:id="A49011-001-a-1030">for</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-1040">our</w>
     <w lemma="sake" pos="n2" xml:id="A49011-001-a-1050">sakes</w>
     <pc xml:id="A49011-001-a-1060">,</pc>
     <w lemma="expose" pos="vvn" xml:id="A49011-001-a-1070">exposed</w>
     <w lemma="his" pos="po" xml:id="A49011-001-a-1080">His</w>
     <hi xml:id="A49011-e180">
      <w lemma="royal" pos="j" xml:id="A49011-001-a-1090">Royal</w>
      <w lemma="person" pos="n1" xml:id="A49011-001-a-1100">Person</w>
     </hi>
     <pc rend="follows-hi" xml:id="A49011-001-a-1110">,</pc>
     <w lemma="even" pos="av" xml:id="A49011-001-a-1120">even</w>
     <w lemma="to" pos="acp" xml:id="A49011-001-a-1130">to</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-1140">the</w>
     <w lemma="admiration" pos="n1" xml:id="A49011-001-a-1150">Admiration</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-1160">of</w>
     <w lemma="his" pos="po" xml:id="A49011-001-a-1170">His</w>
     <w lemma="enemy" pos="n2" xml:id="A49011-001-a-1180">Enemies</w>
     <pc xml:id="A49011-001-a-1190">,</pc>
     <w lemma="do" pos="vvb" xml:id="A49011-001-a-1200">do</w>
     <w lemma="hearty" pos="av-j" xml:id="A49011-001-a-1210">Heartily</w>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-1220">and</w>
     <w lemma="unfeigned" pos="av-j" reg="Unfeignedly" xml:id="A49011-001-a-1230">Vnfeignedly</w>
     <pc xml:id="A49011-001-a-1240">,</pc>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-1250">in</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-1260">the</w>
     <w lemma="first" pos="ord" xml:id="A49011-001-a-1270">first</w>
     <w lemma="place" pos="n1" xml:id="A49011-001-a-1280">place</w>
     <pc xml:id="A49011-001-a-1290">,</pc>
     <w lemma="return" pos="vvb" xml:id="A49011-001-a-1300">return</w>
     <w lemma="to" pos="acp" xml:id="A49011-001-a-1310">to</w>
     <w lemma="almighty" pos="j" xml:id="A49011-001-a-1320">Almighty</w>
     <w lemma="GOD" pos="nn1" rend="hi" xml:id="A49011-001-a-1330">GOD</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-1340">our</w>
     <w lemma="most" pos="avs-d" xml:id="A49011-001-a-1350">most</w>
     <w lemma="humble" pos="j" xml:id="A49011-001-a-1360">Humble</w>
     <w lemma="thanks" pos="n2" xml:id="A49011-001-a-1370">Thanks</w>
     <pc xml:id="A49011-001-a-1380">;</pc>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-1390">and</w>
     <pc xml:id="A49011-001-a-1400">,</pc>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-1410">in</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-1420">the</w>
     <w lemma="next" pos="ord" xml:id="A49011-001-a-1430">next</w>
     <pc xml:id="A49011-001-a-1440">,</pc>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-1450">in</w>
     <w lemma="all" pos="d" xml:id="A49011-001-a-1460">all</w>
     <w lemma="humility" pos="n1" xml:id="A49011-001-a-1470">Humility</w>
     <pc xml:id="A49011-001-a-1480">,</pc>
     <w lemma="congratulate" pos="vvi" xml:id="A49011-001-a-1490">Congratulate</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-1500">Your</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="A49011-001-a-1510">Majesty</w>
     <w lemma="upon" pos="acp" xml:id="A49011-001-a-1520">upon</w>
     <w lemma="so" pos="av" xml:id="A49011-001-a-1530">so</w>
     <w lemma="sensible" pos="j" xml:id="A49011-001-a-1540">Sensible</w>
     <w lemma="a" pos="d" xml:id="A49011-001-a-1550">a</w>
     <w lemma="providence" pos="n1" xml:id="A49011-001-a-1560">Providence</w>
     <pc xml:id="A49011-001-a-1570">,</pc>
     <w lemma="as" pos="acp" xml:id="A49011-001-a-1580">as</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-1590">the</w>
     <w lemma="preservation" pos="n1" xml:id="A49011-001-a-1600">Preservation</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-1610">of</w>
     <w lemma="that" pos="d" xml:id="A49011-001-a-1620">that</w>
     <w lemma="prince" pos="n1" rend="hi" xml:id="A49011-001-a-1630">Prince</w>
     <pc xml:id="A49011-001-a-1640">,</pc>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-1650">in</w>
     <w lemma="who" pos="crq" xml:id="A49011-001-a-1660">whose</w>
     <w lemma="life" pos="n1" xml:id="A49011-001-a-1670">Life</w>
     <pc xml:id="A49011-001-a-1680">,</pc>
     <w lemma="not" pos="xx" xml:id="A49011-001-a-1690">not</w>
     <w lemma="only" pos="av-j" xml:id="A49011-001-a-1700">only</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-1710">our</w>
     <w lemma="law" pos="n2" rend="hi" xml:id="A49011-001-a-1720">Laws</w>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-1730">and</w>
     <w lemma="religion" pos="n1" rend="hi" xml:id="A49011-001-a-1740">Religion</w>
     <pc xml:id="A49011-001-a-1750">,</pc>
     <w lemma="but" pos="acp" xml:id="A49011-001-a-1760">but</w>
     <w lemma="even" pos="av" xml:id="A49011-001-a-1770">even</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-1780">the</w>
     <w lemma="liberty" pos="n1" rend="hi" xml:id="A49011-001-a-1790">Liberty</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-1800">of</w>
     <w lemma="all" pos="d" xml:id="A49011-001-a-1810">all</w>
     <w lemma="Europe" pos="nn1" rend="hi" xml:id="A49011-001-a-1820">Europe</w>
     <pc xml:id="A49011-001-a-1830">,</pc>
     <w lemma="be" pos="vvz" xml:id="A49011-001-a-1840">is</w>
     <w lemma="so" pos="av" xml:id="A49011-001-a-1850">so</w>
     <w lemma="entire" pos="av-j" reg="entirely" xml:id="A49011-001-a-1860">intirely</w>
     <w lemma="wrap" pos="vvn" reg="wrapped" xml:id="A49011-001-a-1870">wrapp'd</w>
     <w lemma="up" pos="acp" xml:id="A49011-001-a-1880">up</w>
     <pc unit="sentence" xml:id="A49011-001-a-1890">.</pc>
    </p>
    <p xml:id="A49011-e260">
     <w lemma="and" pos="cc" xml:id="A49011-001-a-1900">And</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-1910">Your</w>
     <w lemma="majesty" pos="n1" xml:id="A49011-001-a-1920">Majesty</w>
     <w lemma="have" pos="vvg" xml:id="A49011-001-a-1930">having</w>
     <w lemma="be" pos="vvn" xml:id="A49011-001-a-1940">been</w>
     <w lemma="gracious" pos="av-j" xml:id="A49011-001-a-1950">Graciously</w>
     <w lemma="please" pos="vvn" xml:id="A49011-001-a-1960">Pleased</w>
     <w lemma="by" pos="acp" xml:id="A49011-001-a-1970">by</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-1980">the</w>
     <hi xml:id="A49011-e270">
      <w lemma="lord" pos="n1" xml:id="A49011-001-a-1990">Lord</w>
      <w lemma="keeper" pos="n1" xml:id="A49011-001-a-2000">Keeper</w>
     </hi>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-2010">to</w>
     <w lemma="signify" pos="vvi" reg="signify" xml:id="A49011-001-a-2020">signifie</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-2030">the</w>
     <w lemma="deep" pos="j" xml:id="A49011-001-a-2040">deep</w>
     <w lemma="sense" pos="n1" xml:id="A49011-001-a-2050">Sense</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-2060">Your</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="A49011-001-a-2070">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A49011-001-a-2080">hath</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-2090">of</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-2100">the</w>
     <w lemma="great" pos="j" xml:id="A49011-001-a-2110">Great</w>
     <w lemma="loss" pos="n2" xml:id="A49011-001-a-2120">Losses</w>
     <w lemma="at" pos="acp" xml:id="A49011-001-a-2130">at</w>
     <w lemma="sea" pos="n1" xml:id="A49011-001-a-2140">Sea</w>
     <w lemma="that" pos="cs" xml:id="A49011-001-a-2150">that</w>
     <w lemma="have" pos="vvb" xml:id="A49011-001-a-2160">have</w>
     <w lemma="befall" pos="vvn" xml:id="A49011-001-a-2170">befallen</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-2180">the</w>
     <w lemma="trader" pos="n2" xml:id="A49011-001-a-2190">Traders</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-2200">of</w>
     <w lemma="this" pos="d" xml:id="A49011-001-a-2210">this</w>
     <w lemma="city" pos="n1" xml:id="A49011-001-a-2220">City</w>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-2230">and</w>
     <w lemma="kingdom" pos="n1" xml:id="A49011-001-a-2240">Kingdom</w>
     <pc xml:id="A49011-001-a-2250">,</pc>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-2260">and</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-2270">the</w>
     <w lemma="direction" pos="n2" xml:id="A49011-001-a-2280">Directions</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-2290">Your</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="A49011-001-a-2300">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A49011-001-a-2310">hath</w>
     <w lemma="give" pos="vvn" xml:id="A49011-001-a-2320">given</w>
     <w lemma="to" pos="acp" xml:id="A49011-001-a-2330">to</w>
     <w lemma="a" pos="d" xml:id="A49011-001-a-2340">a</w>
     <w lemma="committee" pos="n1" xml:id="A49011-001-a-2350">Committee</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-2360">of</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-2370">Your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" rend="hi" xml:id="A49011-001-a-2380">Majesties</w>
     <w lemma="most" pos="avs-d" xml:id="A49011-001-a-2390">most</w>
     <w lemma="honourable" pos="j" xml:id="A49011-001-a-2400">Honourable</w>
     <w lemma="privy-council" pos="n1" xml:id="A49011-001-a-2410">Privy-Council</w>
     <pc xml:id="A49011-001-a-2420">,</pc>
     <w lemma="as" pos="acp" xml:id="A49011-001-a-2430">as</w>
     <w lemma="well" pos="av" xml:id="A49011-001-a-2440">well</w>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-2450">to</w>
     <w lemma="examine" pos="vvb" xml:id="A49011-001-a-2460">Examine</w>
     <w lemma="into" pos="acp" xml:id="A49011-001-a-2470">into</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-2480">the</w>
     <w lemma="cause" pos="n2" xml:id="A49011-001-a-2490">Causes</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-2500">of</w>
     <w lemma="such" pos="d" xml:id="A49011-001-a-2510">such</w>
     <w lemma="misfortune" pos="n2" xml:id="A49011-001-a-2520">Misfortunes</w>
     <pc xml:id="A49011-001-a-2530">,</pc>
     <w lemma="as" pos="acp" xml:id="A49011-001-a-2540">as</w>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-2550">to</w>
     <w lemma="take" pos="vvi" xml:id="A49011-001-a-2560">take</w>
     <w lemma="effectual" pos="j" xml:id="A49011-001-a-2570">Effectual</w>
     <w lemma="care" pos="n1" xml:id="A49011-001-a-2580">Care</w>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-2590">to</w>
     <w lemma="prevent" pos="vvi" xml:id="A49011-001-a-2600">prevent</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-2610">the</w>
     <w lemma="like" pos="j" xml:id="A49011-001-a-2620">like</w>
     <w lemma="for" pos="acp" xml:id="A49011-001-a-2630">for</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-2640">the</w>
     <w lemma="future" pos="j" xml:id="A49011-001-a-2650">future</w>
     <pc xml:id="A49011-001-a-2660">,</pc>
     <w lemma="by" pos="acp" xml:id="A49011-001-a-2670">by</w>
     <w lemma="encourage" pos="vvg" xml:id="A49011-001-a-2680">Encouraging</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-2690">Your</w>
     <hi xml:id="A49011-e310">
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A49011-001-a-2700">Majesties</w>
      <w lemma="subject" pos="n2" xml:id="A49011-001-a-2710">Subjects</w>
     </hi>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-2720">to</w>
     <w lemma="make" pos="vvi" xml:id="A49011-001-a-2730">make</w>
     <w lemma="their" pos="po" xml:id="A49011-001-a-2740">their</w>
     <w lemma="application" pos="n1" xml:id="A49011-001-a-2750">Application</w>
     <w lemma="to" pos="acp" xml:id="A49011-001-a-2760">to</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-2770">the</w>
     <w lemma="say" pos="j-vn" xml:id="A49011-001-a-2780">said</w>
     <w lemma="committee" pos="n1" xml:id="A49011-001-a-2790">Committee</w>
     <pc unit="sentence" xml:id="A49011-001-a-2800">.</pc>
    </p>
    <p xml:id="A49011-e320">
     <w lemma="we" pos="pns" xml:id="A49011-001-a-2810">We</w>
     <w lemma="do" pos="vvb" xml:id="A49011-001-a-2820">do</w>
     <w lemma="with" pos="acp" xml:id="A49011-001-a-2830">with</w>
     <w lemma="all" pos="d" xml:id="A49011-001-a-2840">all</w>
     <w lemma="cheerfulness" pos="n1" reg="Cheerfulness" xml:id="A49011-001-a-2850">Chearfulness</w>
     <w lemma="render" pos="vvb" xml:id="A49011-001-a-2860">render</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-2870">our</w>
     <w lemma="hearty" pos="j" xml:id="A49011-001-a-2880">Hearty</w>
     <w lemma="thanks" pos="n2" xml:id="A49011-001-a-2890">Thanks</w>
     <w lemma="to" pos="acp" xml:id="A49011-001-a-2900">to</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-2910">Your</w>
     <w lemma="most" pos="avs-d" xml:id="A49011-001-a-2920">most</w>
     <w lemma="gracious" pos="j" xml:id="A49011-001-a-2930">Gracious</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="A49011-001-a-2940">Majesty</w>
     <w lemma="for" pos="acp" xml:id="A49011-001-a-2950">for</w>
     <w lemma="so" pos="av" xml:id="A49011-001-a-2960">so</w>
     <w lemma="great" pos="j" xml:id="A49011-001-a-2970">Great</w>
     <w lemma="a" pos="d" xml:id="A49011-001-a-2980">a</w>
     <w lemma="condescension" pos="n1" reg="Condescension" xml:id="A49011-001-a-2990">Condescention</w>
     <pc xml:id="A49011-001-a-3000">,</pc>
     <w lemma="no" pos="dx" xml:id="A49011-001-a-3010">no</w>
     <w lemma="wise" pos="j" xml:id="A49011-001-a-3020">wise</w>
     <w lemma="doubt" pos="vvg" xml:id="A49011-001-a-3030">doubting</w>
     <w lemma="but" pos="acp" xml:id="A49011-001-a-3040">but</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-3050">Your</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="A49011-001-a-3060">Majesty</w>
     <w lemma="will" pos="vmb" xml:id="A49011-001-a-3070">will</w>
     <w lemma="continue" pos="vvi" xml:id="A49011-001-a-3080">continue</w>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-3090">to</w>
     <w lemma="give" pos="vvi" xml:id="A49011-001-a-3100">give</w>
     <w lemma="such" pos="d" xml:id="A49011-001-a-3110">such</w>
     <w lemma="good" pos="j" xml:id="A49011-001-a-3120">Good</w>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-3130">and</w>
     <w lemma="seasonable" pos="j" xml:id="A49011-001-a-3140">Seasonable</w>
     <w lemma="direction" pos="n2" xml:id="A49011-001-a-3150">Directions</w>
     <pc xml:id="A49011-001-a-3160">,</pc>
     <w lemma="that" pos="cs" xml:id="A49011-001-a-3170">that</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-3180">the</w>
     <w lemma="trade" pos="n1" xml:id="A49011-001-a-3190">Trade</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-3200">of</w>
     <w lemma="this" pos="d" xml:id="A49011-001-a-3210">this</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-3220">Your</w>
     <w lemma="kingdom" pos="n1" rend="hi" xml:id="A49011-001-a-3230">Kingdom</w>
     <pc xml:id="A49011-001-a-3240">,</pc>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-3250">in</w>
     <w lemma="which" pos="crq" xml:id="A49011-001-a-3260">which</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-3270">the</w>
     <w lemma="prosperity" pos="n1" xml:id="A49011-001-a-3280">Prosperity</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-3290">of</w>
     <w lemma="it" pos="pn" xml:id="A49011-001-a-3300">it</w>
     <w lemma="do" pos="vvz" xml:id="A49011-001-a-3310">doth</w>
     <w lemma="so" pos="av" xml:id="A49011-001-a-3320">so</w>
     <w lemma="much" pos="av-d" xml:id="A49011-001-a-3330">much</w>
     <w lemma="consist" pos="vvi" xml:id="A49011-001-a-3340">consist</w>
     <pc xml:id="A49011-001-a-3350">,</pc>
     <w lemma="may" pos="vmb" xml:id="A49011-001-a-3360">may</w>
     <w lemma="be" pos="vvi" xml:id="A49011-001-a-3370">be</w>
     <w lemma="better" pos="avc-j" xml:id="A49011-001-a-3380">better</w>
     <w lemma="support" pos="vvn" xml:id="A49011-001-a-3390">Supported</w>
     <w lemma="for" pos="acp" xml:id="A49011-001-a-3400">for</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-3410">the</w>
     <w lemma="future" pos="j" xml:id="A49011-001-a-3420">future</w>
     <pc unit="sentence" xml:id="A49011-001-a-3430">.</pc>
    </p>
    <p xml:id="A49011-e360">
     <w lemma="and" pos="cc" xml:id="A49011-001-a-3440">And</w>
     <w lemma="as" pos="acp" xml:id="A49011-001-a-3450">as</w>
     <w lemma="we" pos="pns" xml:id="A49011-001-a-3460">we</w>
     <w lemma="have" pos="vvb" xml:id="A49011-001-a-3470">have</w>
     <w lemma="hitherto" pos="av" xml:id="A49011-001-a-3480">hitherto</w>
     <pc xml:id="A49011-001-a-3490">,</pc>
     <w lemma="from" pos="acp" xml:id="A49011-001-a-3500">from</w>
     <w lemma="a" pos="d" xml:id="A49011-001-a-3510">a</w>
     <w lemma="sense" pos="n1" xml:id="A49011-001-a-3520">Sense</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-3530">of</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-3540">our</w>
     <w lemma="duty" pos="n1" xml:id="A49011-001-a-3550">Duty</w>
     <pc xml:id="A49011-001-a-3560">,</pc>
     <w lemma="demonstrate" pos="vvn" xml:id="A49011-001-a-3570">demonstrated</w>
     <w lemma="to" pos="acp" xml:id="A49011-001-a-3580">to</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-3590">the</w>
     <w lemma="whole" pos="j" xml:id="A49011-001-a-3600">whole</w>
     <w lemma="world" pos="n1" xml:id="A49011-001-a-3610">World</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-3620">our</w>
     <w lemma="great" pos="j" xml:id="A49011-001-a-3630">Great</w>
     <w lemma="zeal" pos="n1" xml:id="A49011-001-a-3640">Zeal</w>
     <w lemma="for" pos="acp" xml:id="A49011-001-a-3650">for</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-3660">Your</w>
     <hi xml:id="A49011-e370">
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A49011-001-a-3670">Majesties</w>
      <w lemma="service" pos="n1" xml:id="A49011-001-a-3680">Service</w>
     </hi>
     <pc rend="follows-hi" xml:id="A49011-001-a-3690">;</pc>
     <w lemma="we" pos="pns" xml:id="A49011-001-a-3700">We</w>
     <w lemma="have" pos="vvg" xml:id="A49011-001-a-3710">having</w>
     <w lemma="now" pos="av" xml:id="A49011-001-a-3720">now</w>
     <w lemma="a" pos="d" xml:id="A49011-001-a-3730">a</w>
     <w lemma="fresh" pos="j" xml:id="A49011-001-a-3740">fresh</w>
     <w lemma="opportunity" pos="n1" xml:id="A49011-001-a-3750">Opportunity</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-3760">of</w>
     <w lemma="show" pos="vvg" reg="showing" xml:id="A49011-001-a-3770">shewing</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-3780">the</w>
     <w lemma="same" pos="d" xml:id="A49011-001-a-3790">same</w>
     <pc xml:id="A49011-001-a-3800">,</pc>
     <w lemma="by" pos="acp" xml:id="A49011-001-a-3810">by</w>
     <w lemma="a" pos="d" xml:id="A49011-001-a-3820">a</w>
     <w lemma="cheerful" pos="j" reg="Cheerful" xml:id="A49011-001-a-3830">Chearful</w>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-3840">and</w>
     <w lemma="unanimous" pos="j" reg="unanimous" xml:id="A49011-001-a-3850">Vnanimous</w>
     <w lemma="advance" pos="vvg" xml:id="A49011-001-a-3860">Advancing</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-3870">of</w>
     <w lemma="money" pos="n1" xml:id="A49011-001-a-3880">Money</w>
     <w lemma="for" pos="acp" xml:id="A49011-001-a-3890">for</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-3900">the</w>
     <w lemma="present" pos="j" xml:id="A49011-001-a-3910">Present</w>
     <w lemma="emergency" pos="n2" xml:id="A49011-001-a-3920">Emergencies</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-3930">of</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-3940">Your</w>
     <hi xml:id="A49011-e380">
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A49011-001-a-3950">Majesties</w>
      <w lemma="affair" pos="n2" xml:id="A49011-001-a-3960">Affairs</w>
     </hi>
     <pc rend="follows-hi" xml:id="A49011-001-a-3970">,</pc>
     <w lemma="humble" pos="av-j" xml:id="A49011-001-a-3980">Humbly</w>
     <w lemma="beg" pos="vvb" xml:id="A49011-001-a-3990">beg</w>
     <w lemma="leave" pos="vvi" xml:id="A49011-001-a-4000">Leave</w>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-4010">to</w>
     <w lemma="assure" pos="vvi" xml:id="A49011-001-a-4020">assure</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-4030">Your</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="A49011-001-a-4040">Majesty</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-4050">of</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-4060">our</w>
     <w lemma="firm" pos="j" xml:id="A49011-001-a-4070">Firm</w>
     <w lemma="resolution" pos="n1" xml:id="A49011-001-a-4080">Resolution</w>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-4090">to</w>
     <w lemma="continue" pos="vvi" xml:id="A49011-001-a-4100">continue</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-4110">our</w>
     <w lemma="hearty" pos="j" xml:id="A49011-001-a-4120">Hearty</w>
     <w lemma="endeavour" pos="n2" xml:id="A49011-001-a-4130">Endeavours</w>
     <w lemma="upon" pos="acp" xml:id="A49011-001-a-4140">upon</w>
     <w lemma="all" pos="d" xml:id="A49011-001-a-4150">all</w>
     <w lemma="occasion" pos="n2" xml:id="A49011-001-a-4160">Occasions</w>
     <pc xml:id="A49011-001-a-4170">,</pc>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-4180">to</w>
     <w lemma="support" pos="vvb" xml:id="A49011-001-a-4190">Support</w>
     <w lemma="your" pos="po" xml:id="A49011-001-a-4200">Your</w>
     <hi xml:id="A49011-e400">
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A49011-001-a-4210">Majesties</w>
      <w lemma="royal" pos="j" xml:id="A49011-001-a-4220">Royal</w>
      <w lemma="authority" pos="n1" xml:id="A49011-001-a-4230">Authority</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A49011-001-a-4240">and</w>
     <w lemma="government" pos="n1" rend="hi" xml:id="A49011-001-a-4250">Government</w>
     <pc xml:id="A49011-001-a-4260">,</pc>
     <w lemma="against" pos="acp" xml:id="A49011-001-a-4270">against</w>
     <w lemma="all" pos="d" xml:id="A49011-001-a-4280">all</w>
     <w lemma="person" pos="n2" xml:id="A49011-001-a-4290">Persons</w>
     <pc xml:id="A49011-001-a-4300">,</pc>
     <w lemma="to" pos="prt" xml:id="A49011-001-a-4310">to</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-4320">the</w>
     <w lemma="uttermost" pos="j" xml:id="A49011-001-a-4330">uttermost</w>
     <w lemma="of" pos="acp" xml:id="A49011-001-a-4340">of</w>
     <w lemma="our" pos="po" xml:id="A49011-001-a-4350">our</w>
     <w lemma="power" pos="n1" xml:id="A49011-001-a-4360">Power</w>
     <pc unit="sentence" xml:id="A49011-001-a-4370">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A49011-e420">
   <div type="colophon" xml:id="A49011-e430">
    <p xml:id="A49011-e440">
     <w lemma="print" pos="j-vn" xml:id="A49011-001-a-4380">Printed</w>
     <w lemma="by" pos="acp" xml:id="A49011-001-a-4390">by</w>
     <hi xml:id="A49011-e450">
      <w lemma="Edward" pos="nn1" xml:id="A49011-001-a-4400">Edward</w>
      <w lemma="Jones" pos="nn1" xml:id="A49011-001-a-4410">Jones</w>
     </hi>
     <w lemma="in" pos="acp" xml:id="A49011-001-a-4420">in</w>
     <w lemma="the" pos="d" xml:id="A49011-001-a-4430">the</w>
     <w lemma="Savoy" pos="nn1" rend="hi" xml:id="A49011-001-a-4440">Savoy</w>
     <pc unit="sentence" xml:id="A49011-001-a-4450">.</pc>
     <w lemma="1693." pos="crd" xml:id="A49011-001-a-4460">1693.</w>
     <pc unit="sentence" xml:id="A49011-001-a-4470"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
